const { lstatSync, readdirSync } = require('fs');
const { join } = require('path');

 /**
 * @name getFiles
 * @param {Array<String>} filePaths Array of each file path.
 * @returns {Array<Object>} of object, name and path
 * @throws {Error} on incorrect enviroment and output 
 * @description Gets an array with paths to the files and returns newly created array of objects containing each file's details.
 */

exports.getFiles = function(filePaths) {
  let filesArray = [];
  let slash = '';

  if ( (process.platform === 'darwin') || 
       (process.platform === 'linux') ) {
    slash = '/';
  }
  else if (process.platform === 'win32') {
    slash = '\\';
  } 
  else {
    throw new Error ('The current enviroment is not recognised. Currently supported systems: Windows, Linux and Mac.');
  }
  
  filePaths.map(file => {

  let temp = file.split(slash);  
  let arrayLength = temp.length - 1;
      
    filesArray.push({
      name: temp[arrayLength],
      path: file
    });
  });
  
  if ( filesArray[0].name.includes('/') || filesArray[0].name.includes('\\')) {
    throw new Error ('The entry points are not according to schema.')
  } 
  
  return filesArray;
};

/**
 * Returns an array of paths to all the directories inside of passed direcotry.
 * 
 */
const isDirectory = source => lstatSync(source).isDirectory();
exports.getDirectories = function(source) {
  return readdirSync(source).map(name => join(source, name)).filter(isDirectory);
}
