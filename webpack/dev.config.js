const StyleLintPlugin = require('stylelint-webpack-plugin');

function devBuild(settings) {
  return {
      mode: 'development',
      
      entry: settings.ENTRY_POINT_DEV,
    
      devtool: 'inline-source-map',
    
      devServer: {
        inline: true,
        open: true,
        openPage: 'index.html',
        host: 'localhost',
        port: 3000,
        stats: 'minimal',
      },
    
      module: {
        rules: [
          {
            enforce: 'pre',
            test: /\.js$/,
            exclude: [
              /node_modules/,
              /core/,
              /webpack/
            ],
            loader: 'eslint-loader',
            options: {
              fix: false,
            },
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              'presets': [
                ['env', {
                  'targets': {
                    'browsers': ['last 2 versions', 'ie 11']
                  },
                  'useBuiltIns': true 
                }]
              ]
            }
          },
          {
            test: /\.(scss|css)$/,
            use: [
              { 
                loader: 'style-loader',
                options: {
                  hmr: false,
                  singleton: true,
                } 
              },
              'css-loader',
              'sass-loader'
            ]
          },
          { 
            test: /\.(html)$/,
            use: {
              loader: 'html-loader',
              options: {
                interpolate: true,
                attrs: [
                  'img:src',
                  'img:srcset',
                  'source:srcset'
                ],  
              }
            }
          },
          {
            test: /\.(png|jpg|gif|jpeg)$/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: '[name].[ext]',
                }
              }
            ]
          },
          {
            test: /\.(woff(2)?|ttf|eot|svg)$/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: '[name].[ext]',
              }
            }]
          }
        ]
      },
    
      plugins: [
        new StyleLintPlugin({
          configFile: './scss-lint.yml',
          emitErrors: false,
          // quiet: true,
        }),
      ]
      .concat(settings.INDEX_PLUGIN)
      .concat(settings.HTML_PLUGINS),
  };
}

module.exports = devBuild;
