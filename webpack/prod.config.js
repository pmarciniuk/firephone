const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const { paths } = require('./paths');

// const { join, resolve } = require('path');

function prodBuild(settings) {  
  return {
    entry: settings.ENTRY_POINT_PRODUCTION,
    mode: 'production',
    optimization: {
      minimizer: [
        new UglifyJSPlugin({
          sourceMap: true,
          uglifyOptions: {
            compress: {
              inline: false
            }
          }
        })
      ],
      runtimeChunk: false,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          options: {
            'presets': [
              ['env', {
                'targets': {
                  'browsers': ['last 2 versions', 'ie 11']
                },
                'useBuiltIns': true 
              }]
            ]
          }
        },
        {
          test: /\.(scss|css)$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader'
          ]
        },
        { 
          test: /\.(html)$/,
          use: {
            loader: 'html-loader',
            options: {
              interpolate: true,
              attrs: [
                'img:src',
                'img:srcset',
                'source:srcset'
              ], 
            }
          }
        },
        {
          test: /\.(png|jpg|gif|jpeg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: paths.PROD_ASSETS,
              }
            }
          ]
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                  name: '[name].[ext]',
                  outputPath: paths.ICONS,
            }
          }]
        }
      ]
    },
    plugins: [
      // Clear the old dist folder
      new CleanWebpackPlugin(['dist'], {
        root: process.cwd(),
        verbose: true,
        dry: false
        }),
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
  
      // Webpack's built plugin to define this variable for all our dependencies.
      // Ensure that we get a production build of any dependencies
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production')
        },
      }),
    ]
    .concat(settings.HTML_PLUGINS),
  
    output: {
      path: process.cwd(),
      filename: '[name].js',
      publicPath: paths.PUBLIC_PATH,
    },
  }

}

module.exports = prodBuild;
