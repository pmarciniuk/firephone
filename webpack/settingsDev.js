const HtmlWebpackPlugin = require('html-webpack-plugin');
const { readFileSync } = require('fs');
const { resolve } = require('path');

const settingsCommon = require('./settingsCommon');

/**
 * Config object with all the paths.
 * 
 */
const configDev = {
  INDEX_FILE: 'src/dev/index.html',
  PAGES_DIR: 'src/html'
};

/**
 * Generates the index page HTHMLWebpackPlugin
 * 
 */
function indexPluginGenerator() {

  let template = new HtmlWebpackPlugin({
     templateContent: indexContentGenerator(),
     template: resolve(process.cwd(), configDev.INDEX_FILE),
     inject: false,
     hash: false
   })

   return template;
};

/**
 * Generates the index page content
 * 
 */
function indexContentGenerator() {
  const pageIndex = readFileSync(configDev.INDEX_FILE, { encoding: 'utf8' });

  const pagesLinkArray = 
    settingsCommon.PAGES_ARRAY.map(singlePage => singlePage.name);

  let finalIndex = '';

  pagesLinkArray.forEach(page => {
    finalIndex += `<li><a href="/${page}.html">${page}</a></li>`;
  });

  return pageIndex.replace('{# PAGE_CONTENT #}', finalIndex);
};

/**
 * Creating index.html file for Production environment.
 * 
 */
const indexHtmlPlugin = indexPluginGenerator();

/**
 * Generates an array of HTHMLWebpackPlugins executed for each page.
 * 
 */
function generateHtmlPlugins(templatePages) {

  /** Creates an array for HtmlWebpackPlugin 
    * Changes behaviour depending on @constant {configDev.IS_PRODUCTION}
    * 
    */
   templatePages = templatePages.map(page => { 
    return new HtmlWebpackPlugin({
      inject: true,
      hash: false,
      filename: `${page.name}.html`,
      template: resolve(process.cwd(), `${page.path}/${page.name}.html`),
      chunks: [
        `${page.name}`,
      ]
    })
   });
 
   return templatePages;
 };

 /**
 * Creating HTMLWebpackPlugins array.
 * 
 */
const htmlPlugins = generateHtmlPlugins(settingsCommon.PAGES_ARRAY);

/**
 * Generates an object of entry points for core, components and pages.
 * 
 * @param {Array<Object>} pagesEntry Array of objects containing details about 
 * each page entry file (i.e name and path).
 * 
 */
function entryPointGenerator(pagesEntry) {
  
  pagesEntry = entryPathBuilder(pagesEntry)

  // return Object.assign({}, settingsCommon.CORE, pagesEntry)
  return pagesEntry;
};

/**
 * Generates and returns an array of objects containing desired destination path
 * for each entry file.
 * 
 * @param {Array<Object>} entry Array of objects with file details.
 * 
 */
function entryPathBuilder(entry) {
  
  return entry.reduce((acc, item ) => {
    
    // Entry path to the file (where to pick it up and add it to the graph)
    let entryPath = item.path + '/' + item.name + '.js';
    let distPath = item.name;

    acc[distPath] = resolve(entryPath);
    return acc;
  }, {});
};

/**
 * Creating entry points objects for Development environment.
 * 
 */
const entryObjectDevelopment = entryPointGenerator(settingsCommon.PAGES_ARRAY);

/**
 * Exposed object with generated entry points for both environments and an
 * array wtih HTMLWebpackPlugin for production mode.
 * 
 */
module.exports = {
  ENTRY_POINT_DEV: entryObjectDevelopment,
  HTML_PLUGINS: htmlPlugins,
  INDEX_PLUGIN: indexHtmlPlugin
}
