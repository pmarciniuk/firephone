/**
 * Object with all the paths used.
 * 
 */
exports.paths = {
  PROD_ASSETS: 'dist/assets',
  PUBLIC_PATH: '../..',
  ICONS: 'dist/assets/icons/'
};
