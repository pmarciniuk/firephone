const { resolve } = require('path');
const settingsCommon = require('./settingsCommon');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const utils = require('./utils');

/**
 * Config object with all the paths.
 * 
 */
const configProd = {
  COMPONENTS_DIR: 'src/components',
  PAGES_BUILD: 'dist/html/'
};

/**
 * Getting an array of directories for components.
 * 
 */
const comoponents = utils.getDirectories(configProd.COMPONENTS_DIR);

/**
 * Getting an array of objects with details of each file (components).
 * 
 */
const componentsArray = utils.getFiles(comoponents);

/**
 * Generates an object of entry points for core, components and pages.
 * 
 * @param {Array<Object>} pagesEntry Array of objects containing details about 
 * each page entry file (i.e name and path).
 * @param {Array<Object>} componentsEntry Array of objects containing details about 
 * each component entry file (i.e name and path).
 * 
 */
function entryPointGenerator(pagesEntry, componentsEntry) {
  
  pagesEntry = 
    entryPathBuilder(pagesEntry, true);
  
  componentsEntry = 
    entryPathBuilder(componentsEntry, false);

  // return Object.assign({}, settingsCommon.CORE, componentsEntry, pagesEntry)
  return Object.assign({}, componentsEntry, pagesEntry)
};

/**
 * Generates and returns an array of objects containing desired destination path
 * for each entry file.
 * 
 * @param {Array<Object>} entry Array of objects with file details.
 * @param {Boolean} bundleToDist Will destination path be either dist or src folder.
 * 
 */
function entryPathBuilder(entry, bundleToDist) {
  
  return entry.reduce((acc, item ) => {
    
    // Entry path to the file (where to pick it up and add it to the graph)
    let entryPath = item.path + '/' + item.name + '.js';
    let distPath = item.path + '/' + item.name + '.bundle';
    
    if (bundleToDist) {
      distPath = distPath.replace('src','dist');
    }

    acc[distPath] = resolve(entryPath);
    return acc;
  }, {});
};


/**
 * Creating entry points objects for Production environment.
 * 
 */
const entryObjectProduction = 
  entryPointGenerator(settingsCommon.PAGES_ARRAY, componentsArray);


/**
 * Generates an array of HTHMLWebpackPlugins executed for each page.
 * 
 */
function generateHtmlPlugins(templatePages) {

  /**  
   * Creates an array for HtmlWebpackPlugin
   */
  templatePages = templatePages.map(page => { 
    return new HtmlWebpackPlugin({
      inject: true,
      hash: false,
      filename: `${configProd.PAGES_BUILD}${page.name}/${page.name}.html`,
      template: resolve(process.cwd(), `${page.path}/${page.name}.html`),
      chunks: [
        `${configProd.PAGES_BUILD}${page.name}/${page.name}.bundle`,
      ]
    })
  });

  return templatePages;
 };

 /**
 * Creating HTMLWebpackPlugins array.
 * 
 */
const htmlPlugins = generateHtmlPlugins(settingsCommon.PAGES_ARRAY);

/**
 * Exposed object with generated entry points for both environments and an
 * array wtih HTMLWebpackPlugin for production mode.
 * 
 */
module.exports = {
  ENTRY_POINT_PRODUCTION: entryObjectProduction,
  HTML_PLUGINS: htmlPlugins
}
