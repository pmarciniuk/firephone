const { resolve } = require('path');
const utils = require('./utils');

/**
 * Config object with all the paths.
 * 
 */
const configDirs = {
  CORE_DIR: resolve('./../core/core.js'),
  PAGES_DIR: 'src/html',
  IS_PRODUCTION: process.env.NODE_ENV == 'production'
};

const core = configDirs.IS_PRODUCTION ? 
{'dist/core/bundle': resolve(configDirs.CORE_DIR)} : 
{'core': resolve(configDirs.CORE_DIR)};

/**
 * Getting an array of directories for pages.
 * 
 */
const pages = utils.getDirectories(configDirs.PAGES_DIR);

/**
 * Getting an array of objects with details of each file (pages).
 * 
 */
const pagesArray = utils.getFiles(pages);

/**
 * Exposed object with generated entry points for both environments and an
 * array wtih HTMLWebpackPlugin for production mode.
 * 
 */
module.exports = {
  CORE: core,
  PAGES_ARRAY: pagesArray,
  IS_PRODUCTION: configDirs.IS_PRODUCTION
}
