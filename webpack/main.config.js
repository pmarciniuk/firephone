const settingsProd = require('./settingsProd');
const settingsDev = require('./settingsDev');

function buildConfig() {
  
  switch(process.env.NODE_ENV) {
    case 'development': 
      return require('./dev.config')(settingsDev);
    case 'production':
      return require('./prod.config')(settingsProd);
    default:
      console.log("Wrong webpack build parameter. Possible choices: `dev` or `prod`.");
  }

}

module.exports = buildConfig;
