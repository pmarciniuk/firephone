module.exports = {
  "parserOptions": {
      "ecmaVersion": 8,
      "sourceType": "module"
  },
  "extends": "eslint:recommended",
  "env": {
    "browser": true,
    "node": true
  },
  "rules": {
    // enable additional rules
      // Possible Errors
    "no-await-in-loop": "error",
    "no-extra-parens": "warn",
    "no-prototype-builtins": "warn",
    "no-template-curly-in-string": "warn",
    "accessor-pairs": "warn",
    "array-callback-return": "warn",
    "block-scoped-var": "warn",
    "complexity": "warn",
    "consistent-return": "warn",
      
      // Best Practices
    "default-case": "warn",
    "eqeqeq": ["warn", "smart"],
    
    // enable additional styling rules
    "camelcase": "error",
    "quotes": ["error", "single"],
    "semi": ["error", "always"],
    // "indent": ["error", 2],
    "linebreak-style": ["error", "unix"],
    
    // override default options for rules from base configurations
    "no-console": "warn"
  }
}
